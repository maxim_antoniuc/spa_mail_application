/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.10-MariaDB : Database - email
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`email` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `email_folder` */

CREATE TABLE `email_folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `email_folder` */

insert  into `email_folder`(`id`,`name`,`created_date`,`alias`) values (1,'INBOX','2016-03-31 01:41:24','Inbox');
insert  into `email_folder`(`id`,`name`,`created_date`,`alias`) values (2,'[Gmail]/Spam','2016-03-31 01:41:27','Spam');
insert  into `email_folder`(`id`,`name`,`created_date`,`alias`) values (3,'[Gmail]/Trash','2016-03-31 01:41:35','Trash');

/*Table structure for table `emails` */

CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folder_id` int(11) DEFAULT NULL,
  `g_from` text,
  `g_subject` text,
  `g_message` text,
  `g_date` timestamp NULL DEFAULT NULL,
  `dmarc` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `g_id` int(11) DEFAULT NULL,
  `g_seen` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key_g` (`g_id`),
  KEY `foreign_key_folder_1` (`folder_id`),
  CONSTRAINT `foreign_key_folder_1` FOREIGN KEY (`folder_id`) REFERENCES `email_folder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=879 DEFAULT CHARSET=utf8;

/*Data for the table `emails` */

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
