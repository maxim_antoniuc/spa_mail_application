<?php

/**display params
 * @param $string
 */
function dd($string)
{
    echo "<pre>";
    var_dump($string);
    echo "</pre>";
    die;
}


/**checks for dmark
 * @param $string
 * @return bool
 */
function isDMARC($string)
{

    $pos = strpos(strtolower($string), 'dmarc=pass');
    return $pos !== false ? 1 : 0;

}

/**checks params 
 * @param $var
 * @return bool
 */
function checkVar(&$var)
{
    return (isset($var) && !empty($var));
}

/**encode tool
 * @param $array
 * @return string
 */
function simple_encode($array)
{
    return base64_encode(json_encode($array));
}

/**decode tool
 * @param $string
 * @return mixed
 */
function simple_decode($string){
    return json_decode(base64_decode($string));
}