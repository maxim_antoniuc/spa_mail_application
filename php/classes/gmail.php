<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/config/email.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/classes/dbClass.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/helper/functions.php";

/**
 * Class gmail
 */
class gmail
{
    protected $mailbox;
    protected $email;
    protected $password;
    protected $pdo;

    /**
     * gmail constructor.
     * @param $email
     * @param $password
     */
    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
        $this->pdo = new dbClass();
    }

    /**connection
     * @param $folder
     * @return resource
     */
    protected function connect($folder)
    {
        return imap_open("{" . MAIL_ADDRESS . ":" . MAIL_PORT . "/" . MAIL_TYPE . "}" . $folder, $this->email, $this->password);

    }

    public function testConnection()
    {
        $imap = $this->connect('INBOX');
        return (imap_errors() === false);
    }


    /**get emails
     * @param $folder
     * @param $folder
     * @return array
     */
    public function syncEmails($folder_id)
    {
        $folder = $this->pdo->getEmailFolder($folder_id);

        $inbox = $this->connect($folder);

        $output = false;

        // imap_setflag_full($mailbox, $mail[0], "\\Seen \\Flagged");

        $date = date("j-M-Y", strtotime("-7 days"));
        $emails = imap_search($inbox, 'SINCE ' . $date);

        if ($emails) {
            $em_array = [];

            //sort by newest
            rsort($emails);

            //iterate through emails
            foreach ($emails as $email_number) {

                $emails_processed = [];

                $emails_processed['folder_id'] = $folder_id;

                $overview = imap_fetch_overview($inbox, $email_number, 0);
                $structure = imap_fetchstructure($inbox, $email_number);

                $emails_processed['g_id'] = $overview[0]->uid;

                $raw = imap_fetchbody($inbox, $email_number, "");
                $emails_processed['dmarc'] = isDMARC($raw);


                if (isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
                    $part = $structure->parts[1];
                    $message = imap_fetchbody($inbox, $email_number, 2);

                    if ($part->encoding == 3) {
                        $emails_processed['g_message'] = imap_base64($message);
                    } else if ($part->encoding == 1) {
                        $emails_processed['g_message'] = imap_8bit($message);
                    } else {
                        $emails_processed['g_message'] = imap_qprint($message);
                    }
                }

                $emails_processed['g_seen'] = $overview[0]->seen ? 1 : 0;
                $emails_processed['g_from'] = imap_utf8($overview[0]->from);
                $emails_processed['g_date'] = date("Y-m-d H:i:s", strtotime(utf8_decode(imap_utf8($overview[0]->date))));
                $emails_processed['g_subject'] = imap_utf8($overview[0]->subject);

                $em_array[] = $emails_processed;
            }


            //insert into db
            $output = $this->pdo->insertBatch('emails', $em_array);
        }

        imap_close($inbox);

        return $output;
    }

    /**delete
     * @param $folder
     * @param $id
     */
    function deleteMessage($id)
    {

        $email = $this->pdo->getByColumn('emails', 'g_id', $id);

    
        if ($email) {

            $folder = $this->pdo->getEmailFolder($email->folder_id);

            $inbox = $this->connect($folder);
            

            imap_delete($inbox, $id , FT_UID);

            $check = imap_mailboxmsginfo($inbox);
            $this->pdo->delete('emails','g_id',$id);

            //set flag
            imap_expunge($inbox);

            imap_close($inbox);
            return true;

        }

        return false;


    }
}