<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/php/config/db.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/classes/gmail.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/helper/functions.php';


/**
 * Description of emailClass
 *
 * @author max1
 */
class emailClass
{
    protected $pdo;

    /**
     * gmail constructor.
     * @param $email
     * @param $password
     */
    public function __construct()
    {
        $this->pdo = new dbClass();
    }

    /**
     * login
     */
    public function register()
    {

        if (!count($_POST)) return FALSE;

        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);

        $gmail = new gmail($email, $password);

        $test = $gmail->testConnection();
        if ($test) {

            $data_processed['email'] = $email;
            $data_processed['password'] = simple_encode(['email' => $email, 'password' => $password]);


            $_SESSION['current_user'] = $email;

            //insert into db
            $output = $this->pdo->insertBatch('users', [$data_processed],FALSE);

            $fetch = $this->fetchEmails();


            return TRUE;
        }

        return false;

    }

    /**
     * @return array
     */
    public function fetchEmails()
    {
        $out = [];

        if (checkVar($_SESSION['current_user'])) {

            $data = $this->pdo->getByColumn('users', 'email', $_SESSION['current_user']);

            if (checkVar($data)) {
                $dec = simple_decode($data->password);

                $gmailClient = new gmail($data->email, $dec->password);
                $folders = $this->pdo->getAll('email_folder');

                foreach ($folders as $folder) {
                    $out[] = $gmailClient->syncEmails($folder->id);
                }

            }

        }

        return $out;
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        if (checkVar($_SESSION['current_user'])) {

            $data = $this->pdo->getByColumn('users', 'email', $_SESSION['current_user']);
            $dec = simple_decode($data->password);
            $gmailClient = new gmail($data->email, $dec->password);

            return $gmailClient->deleteMessage($id);
        }
        else{
            return FALSE;
        }
    }


}
