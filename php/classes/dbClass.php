<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/config/db.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/helper/functions.php";

class dbClass
{
    protected $db;

    public function __construct()
    {
        try {
            $this->db = new PDO("mysql:host=" . dbhost . ";dbname=" . dbname, dbuser, dbpswd);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $this->db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES UTF8');
        } catch (PDOException $e) {
            throw new PDOException("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $table
     * @return array
     */
    public function getAll($table)
    {
        $sql = "select * from {$table} ";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param $id
     * @return null|string
     */
    public function getEmailFolder($id)
    {

        $sql = "Select name from email_folder WHERE id = :id";

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return $stmt ? $stmt->fetchColumn() : NULL;


    }

    /**
     * @param $table
     * @param $array
     * @return int
     */
    public function insertBatch($table, $array, $encode = TRUE)
    {
        $insertData = array();
        $rez = array();

        //gets cols name
        $headerKey = array_keys($array[0]);
        $header = implode(',', $headerKey);

        foreach ($headerKey as &$array_key) {
            $array_key = ":{$array_key}";
        }

        $headerKey = implode(',', $headerKey);

        $sql = "INSERT INTO {$table} ($header) VALUES ";

        foreach ($array as $key => $value) {

            $insertQuery[] = "($headerKey)";
            foreach ($value as $col => $item) {
                $rez[":$col"] = $encode ? urlencode($item) : $item;
            }
            $insertData[] = $rez;
        }


        if (!empty($headerKey)) {

            $sql .= "($headerKey)";
            if ($table == 'emails') {
                $sql .= " ON DUPLICATE KEY UPDATE g_id=g_id";
            } elseif ($table == 'users') {
                $sql .= " ON DUPLICATE KEY UPDATE email=email";
            }
            $stmt = $this->db->prepare($sql);

            foreach ($insertData as $value) {

                $stmt->execute($value);
            }
        }

        return $stmt->rowCount();
    }

    /**
     * @param $table
     * @param $col
     * @param $val
     * @return mixed
     */
    public function getByColumn($table, $col, $val)
    {
        $sql = "select * from {$table} where {$col}=?";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(1, $val);
        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * @param $id
     * @return array
     *
     */
    public function getEmailsByFolderId($id)
    {
        $sql = "select * from `emails` where `folder_id`=?";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(1, $id);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param $table
     * @param $col
     * @param $val
     */
    public function delete($table, $col, $val)
    {

        $sql = "delete  from {$table} where {$col}=?";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(1, $val);
        $stmt->execute();

    }

}