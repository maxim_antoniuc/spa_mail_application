<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/classes/dbClass.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/helper/functions.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/classes/gmail.php";

$dbObj = new dbClass();
$id = isset($_GET['id']) ? (int)$_GET['id'] : 1;
$emails = $dbObj->getEmailsByFolderId($id);
$folder = $dbObj->getByColumn('email_folder','id',$id);
?>

<div class="box box-primary gmail-content">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$folder->alias?></h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="mailbox-controls">

        </div>
        <div class="table-responsive mailbox-messages">

            <?php
            if (checkVar($emails)) {
                ?>
                <table class="table table-hover table-striped">
                    <tbody>
                    <?php
                    foreach ($emails as $key => $email) {
                        ?>
                        <tr attr-id="<?= $email->g_id ?>">
                            <td>
                                    <button class="btn btn-default delete btn-sm" onclick="deleteMail(this, <?= $email->g_id ?>);" attr-id = "<?= $email->g_id ?>"><i class="fa fa-trash-o"></i></button></td>
                            <td class="mailbox-name"><a
                                    href="read-mail.html"><?= urldecode($email->g_from) ?></a></td>
                            <td class="mailbox-subject"><?= urldecode($email->g_subject) ?></td>
                            <td class="mailbox-date"><?php if ($email->dmarc == 1) { ?><span
                                    style="color: green;">dmarc=true</span><?php } else { ?><span
                                    style="color: red;">dmarc=false</span><?php } ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table><!-- /.table -->
                <?php
            }
            else{
            ?>
                <h4 class="text-center">No emails</h4>
            <?php
            }
            ?>

        </div><!-- /.mail-box-messages -->
    </div><!-- /.box-body -->
    <div class="box-footer no-padding">
        <div class="mailbox-controls"></div>
    </div>
</div>


