<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/classes/dbClass.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/classes/emailClass.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/helper/functions.php";

$dbObj = new dbClass();


//get mail

if (isset($_GET['gid'])){

$gid = (int)$_GET['gid'];
$email = $dbObj->getByColumn('emails', 'g_id', $gid);
?>

<div class="box-header">
    <h3 class="box-title">Read Mail</h3>
</div><!-- /.box-header -->
<div class="box-body no-padding">
    <?php if (checkVar($email)) { ?>
        <div class="mailbox-read-info">
            <h3><?= urldecode($email->g_subject) ?> <?php if ($email->dmarc == 1) { ?><span
                    style="color: green;">dmarc=true</span><?php } else { ?><span
                    style="color: red;">dmarc=false</span><?php } ?></h3>
            <h5>From: <?= urldecode($email->g_from) ?> <span
                    class="mailbox-read-time pull-right"><?= $email->g_date ?></span></h5>
        </div><!-- /.mailbox-read-info -->
        <div class="mailbox-read-message">
            <?= urldecode($email->g_message) ?>
        </div><!-- /.mailbox-read-message -->
    <?php } ?>
</div><!-- /.box-body -->
<!-- /.box-footer -->
<?php
}


if(isset($_POST['delete'])){

    $email = new emailClass();
    $email->delete((int)$_POST['delete']);

}