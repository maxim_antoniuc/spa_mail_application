/**
 * check sessiona at startup
 */
$(document).ready(function () {
    checkSession();

    $(document).ajaxStart(function () {
        $("#loading").show();
    }).ajaxStop(function () {
        $("#loading").hide();
    });

})


/**
 * login form
 */
$(document).on("submit", ".login-box-body form", function (e) {
    var form = $(this).serialize();

    $.post("../public/mailbox.php?id=1", form, function (data) {

        checkSession(function () {

            $("body div#custom-wrapper").find('.login-box-msg').html('Gmail connection failed! Please Check your credentials');


        });
    });

    e.preventDefault();
});

/**
 * show email details
 */
$(document).on("click", "div.box-primary table tr .mailbox-name", function (e) {
    e.preventDefault();

    var gid = $(this).parent().attr('attr-id');

    $.get("../public/mail.php?gid=" + gid, function (data) {
        $(".gmail-content").html(data);
    });

});

/**
 * gets mailbox emails
 */

$(document).on("click", "div.box-body li a", function (e) {
    e.preventDefault();

    var id = $(this).attr('attr-id');

    $.get("../public/inbox.php?id=" + id, function (data) {
        $(".gmail-content").html(data);
    });

});


/**
 *checks php session
 * @param cb
 */
function checkSession(cb) {

    $.getJSON("session.php", function (data) {

        if (data.session === true) {
            $.get("../public/mailbox.php?id=1", function (data) {
                $("body div#custom-wrapper").replaceWith(data);

            })
        }
        else {

            $.get("../public/send-form.php", function (data) {
                $("body div#custom-wrapper").replaceWith(data);

                if (typeof cb === "function") {
                    cb()
                }

            })
        }
    });
}

/**
 *delete a mail
 * @param param
 * @param id
 */
function deleteMail(param, id) {

    var tr = param.parentElement.parentElement;

    if (confirm('Are You Sure?')) {

        $.post("../public/mail.php", {'delete': id}, function () {
            tr.remove();
        });
    }
}
