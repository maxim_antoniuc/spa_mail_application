<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/helper/functions.php";

session_start();
session_destroy();
if(isset($_GET['exit'])){
    session_destroy();
}

echo json_encode([
    'session' => isset($_SESSION['current_user'])
]);

